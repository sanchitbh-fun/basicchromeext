const yargs = require('yargs');

let args = yargs
    .option('name', {default: 'plugin', describe: 'name of plugin'})
    .option('desc', {default: 'description', describe: 'description of plugin'})
    .option('popup', {boolean: true, describe: 'create popup menu'})
    .option('options', {boolean: true, describe: 'create options page'})
    .option('bg', {boolean: true, describe: 'create background page'})
    .argv;

module.exports = {};
for (let key in args) {
    module.exports[key] = args[key];
}
