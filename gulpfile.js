const gulp = require('gulp');
const mustache = require('gulp-mustache');
const minifyJson = require('gulp-jsonminify');
const minifyJs = require('gulp-uglify');
const ts = require('gulp-typescript');
const del = require('del');
const seq = require('gulp-sequence');
const babel = require('gulp-babel');
const webpack = require('webpack-stream');
const merge = require('merge2');
const named = require('vinyl-named-with-path');

const args = require('./args');
const DIST_DIR = './dist';

gulp.task('default', ['clean', 'assets', 'locales', 'manifest', 'typescript', 'watcher']);

gulp.task('clean', function () {
    del.sync(DIST_DIR);
});

gulp.task('manifest', function () {
    return gulp.src('./manifest.json')
        .pipe(mustache(args))
        .pipe(gulp.dest(DIST_DIR));
});

gulp.task('typescript', function () {
    let tsResult = gulp.src("src/**/*.ts")
        .pipe(named())
        .pipe(webpack({
            resolve: {
                alias: {
                    'vue$': 'vue/dist/vue.esm.js' // 'vue/dist/vue.common.js' for webpack 1
                }
            },
            module: {
                loaders: [
                    {test: /\.ts$/, loader: 'ts-loader'},
                    {test: /\.vue$/, loader: 'vue-loader'},
                ]
            }
        }))
        .pipe(gulp.dest(DIST_DIR + '/src'));
});

gulp.task('assets', function () {
    return gulp.src(['src/**/*.vue', 'src/**/*.html', 'src/**/*.png'])
        .pipe(gulp.dest(DIST_DIR + '/src'));
});

gulp.task('locales', function () {
    return gulp.src('_locales/**')
        .pipe(gulp.dest(DIST_DIR + '/_locales'));
});

gulp.task('watcher', function () {
    gulp.watch('src/**/*.ts', ['typescript']);
});
